.PHONY: app
app:
	docker-compose run --rm --service-ports app /bin/bash

web:
	docker-compose run --rm --service-ports web /bin/bash
