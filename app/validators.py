from email_validator import EmailNotValidError
from email_validator import validate_email as external_validate_email
from webargs import ValidationError, fields

from models import User


def validate_name(name):
    if len(name) < 2:
        raise ValidationError("Too short.")
    if not name.isalnum():
        raise ValidationError("Invalid.")
    if User.query.filter_by(name=name).first():
        raise ValidationError("Taken.")


def validate_email(email, **kwargs):
    if User.query.filter_by(email=email).first():
        raise ValidationError("Taken.")

    try:
        return external_validate_email(email, **kwargs)
    except EmailNotValidError as e:
        raise ValidationError(str(e))


def validate_password(password):
    if len(password) < 6:
        raise ValidationError("Too short.")
    # TODO: complexity?
