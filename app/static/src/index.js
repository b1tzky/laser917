import React, { useState, useEffect, useCallback } from "react";
import { render } from "react-dom";
import { Routes, Route, Outlet, Link } from "react-router-dom";

import Index from "./js/index.js";

import "./scss/index.scss";

// render(<Index />, document.getElementById("car-index"));

const Gallery = ({ visibleState, data }) => {
  const { images } = data;
  const [visible, setVisible] = visibleState;

  const [activeImage, setActiveImage] = useState(images.find(Boolean));

  const nextImage = () => {
    console.log(images.find((i) => i.path === activeImage.path));
  };
  const previousImage = () => {};

  if (images.length === 0) {
    setVisible(false);
  }

  const escFunction = useCallback((event) => {
    if (event.keyCode === 27) {
      setVisible(false);
    }
  });

  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);
    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  });

  return (
    <div className="gallery">
      <div className="close" onClick={() => setVisible(false)}>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
          <path d="m24 20.188-8.315-8.209 8.2-8.282L20.188 0l-8.212 8.318L3.666.115 0 3.781l8.321 8.24-8.206 8.313L3.781 24l8.237-8.318 8.285 8.203z" />
        </svg>
      </div>
      <div className="lightbox">
        {activeImage && <img src={`/media/${activeImage.path}`} />}
      </div>
      <div className="scrubber">
        {images.map((i) => {
          const active = i.path == activeImage.path ? "active" : "";
          return (
            <div
              className={`image ${active}`}
              onClick={() => setActiveImage(i)}
            >
              <img src={`/media/${i.thumbnail}`} />
            </div>
          );
        })}
      </div>
      <div className="prev"></div>
      <div className="next"></div>
    </div>
  );
};

const Car = ({ matches, ...props }) => {
  const { id } = matches;
  const [data, setData] = useState();
  const galleryVisibleState = useState(false);
  const [galleryVisible, setGalleryVisible] = galleryVisibleState;
  const toggleGalleryVisible = () => setGalleryVisible(!galleryVisible);

  useEffect(() => setGalleryVisible(false), [id]);

  useEffect(() => {
    fetch(`/api/car/${id}`)
      .catch()
      .then((response) => response.json())
      .then((r) => {
        setData(r);
      });
  }, [id]);

  if (!data) {
    return <></>;
  }

  const { car, images } = data;
  const headImage = images.find((r) => r.hero) || images[0] || undefined;

  return (
    <div className="stage">
      <header>
        <div className="hero" onClick={toggleGalleryVisible}>
          <div className="image">
            {headImage && <img src={`/media/${headImage.thumbnail}`} />}
          </div>

          <div className="text">{images.length} images</div>
        </div>

        <h2>
          {car.slug}
          {car.model && <small>Model {car.model}</small>}
        </h2>
      </header>

      <section className="about">{car.description}</section>

      {galleryVisible && (
        <Gallery visibleState={galleryVisibleState} data={data} />
      )}
    </div>
  );
};

const App = () => {
  // const stuff = document.getElementById("stuff")?.innerHTML || "{}"

  // const index = JSON.parse(stuff);
  // const models = Object.keys(index);

  return (
    <div className="shell">
      <div
        className="d-flex flex-column flex-shrink-0 text-white bg-dark"
        style={{ width: "280px" }}
      >
        <div className="sticky-header p-2">
          <a
            href="/"
            className="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none"
          >
            <span className="fs-4">Laser 917</span>
          </a>
        </div>
        <ul className="nav nav-pills p-2 flex-column mb-auto">
          <Index />
        </ul>
      </div>

      <main role="main">
        <Routes>
          <Route path="/car/:slug" element={<Car />} />
        </Routes>
      </main>
    </div>
  );
};

import { BrowserRouter } from "react-router-dom";

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("mount")
);
