import json

from models import User
from utils import random_string

from conftest import client_with_jwt


def test_not_logged_in(client):
    resp = client.get("/api/auth/me")
    assert int(resp.status_code) == 401
    assert resp.json["msg"].startswith("Missing cookie ")


def test_bad_registration_info(client):
    good_name = random_string(numwords=1)
    good_email = f"{good_name}@example.com"
    good_password = "party!"

    def bad_registration_attempt(
        field, name=good_name, email=good_email, password=good_password
    ):
        resp = client.put(
            "/api/auth/register",
            data=json.dumps(
                {
                    "name": name,
                    "email": email,
                    "password": password,
                }
            ),
            content_type="application/json",
        )
        assert int(resp.status_code) == 422
        assert resp.json["fields"].get(field, False) is not False

    for field, values in {
        "name": ["a", "!@#%",],
        "email": ["notcomplete", "no@tldext",],
        "password": ["1",],
    }.items():
        for v in values:
            bad_registration_attempt(field, **{field: v})


def test_registration(client):
    name = random_string(numwords=1)
    password = "party!"

    resp = client.put(
        "/api/auth/register",
        data=json.dumps(
            {
                "name": name,
                "email": f"{name}@example.com",
                "password": password,
            }
        ),
        content_type="application/json",
    )
    assert int(resp.status_code) == 200
    assert resp.json["msg"] == "ok"

    registered = User.query.filter_by(name=name).first()
    assert registered.enabled == False
    assert registered.email_token is not None

    resp = client.post(
        "/api/auth/verify",
        data=json.dumps(
            {
                "token": registered.email_token,
            }
        ),
        content_type="application/json",
    )
    assert int(resp.status_code) == 200
    assert resp.json["msg"] == "ok"

    verified = User.query.filter_by(name=name).first()
    assert verified.enabled == True
    assert verified.email_token is None


def test_double_registration(client, enabled_user):
    resp = client.put(
        "/api/auth/register",
        data=json.dumps(
            {
                "name": enabled_user.name,
                "email": enabled_user.email,
            }
        ),
        content_type="application/json",
    )
    assert int(resp.status_code) == 422
    assert len(resp.json["msg"]) > 0
    assert list(resp.json["fields"].keys()) == ["email", "name", "password"]


def test_login(client, enabled_user):
    resp = client.post(
        "/api/auth/login",
        data=json.dumps(
            {
                "name": enabled_user.name,
                "password": "party!",
            }
        ),
        content_type="application/json",
    )
    assert resp.json["name"] == enabled_user.name
    assert resp.headers.get("Set-Cookie").startswith("access_token_cookie=")

    resp = client_with_jwt(client, enabled_user).get(
        "/api/auth/me",
        content_type="application/json",
    )
    assert resp.json["name"] == enabled_user.name
