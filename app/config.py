import os

DEBUG = os.environ.get("DEBUG", False)

UPLOAD_FOLDER = "./media/"

SECRET_KEY = os.environ.get("SECRET_KEY", "shh")
SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
SECURITY_PASSWORD_SALT = os.environ.get(
    "SECURITY_PASSWORD_SALT", "ATGUOHAELKiubahiughaerGOJAEGj"
)
SECURITY_REGISTERABLE = False
SECURITY_SEND_REGISTER_EMAIL = False

# SQLALCHEMY_ECHO = True
SQLALCHEMY_DATABASE_URI = "postgresql://{u}:{p}@{h}/{n}".format(
    u=os.environ.get("DB_USER", "dan"),
    p=os.environ.get("DB_PASSWORD", "party!"),
    h=os.environ.get("DB_HOST", "postgres"),
    n=os.environ.get("DB_NAME", "laser917"),
)
SQLALCHEMY_TRACK_MODIFICATIONS = False
