from datetime import datetime, timedelta, timezone
from functools import wraps

from flask import jsonify
from flask_jwt_extended import (JWTManager, create_access_token, get_jwt,
                                get_jwt_identity, jwt_required,
                                set_access_cookies, unset_jwt_cookies,
                                verify_jwt_in_request)

from models import User


def with_user(**outer_kwargs):
    def wrapper(fn):
        @wraps(fn)
        @jwt_required(**outer_kwargs)
        def decorator(*args, **kwargs):
            jwt = get_jwt()
            if slug := jwt.get("sub", False):
                user = User.query.filter(User.slug == slug).first()
                if user and user.enabled:
                    kwargs["user"] = user
                else:
                    # user had a valid jwt but a bad slug
                    response = jsonify(msg="Bad token.")
                    unset_jwt_cookies(response)
                    return response, 401

            response, status = fn(*args, **kwargs)
            if type(response) == dict:
                response = jsonify(response)

            # if theres a user, and the response was good
            if "user" in kwargs and status == 200:
                # autoregenerate access token
                if expiration := jwt.get("exp", False):
                    # if the timedelta is ahead of the expiration
                    target = datetime.timestamp(
                        datetime.now(timezone.utc) + timedelta(minutes=30)
                    )
                    if target > expiration:
                        set_access_cookies(
                            response,
                            create_access_token(identity=kwargs["user"].slug),
                        )
            return response, status

        return decorator

    return wrapper
