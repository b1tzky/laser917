import enum
from datetime import datetime

# from flask_jwt_extended import create_access_token, create_refresh_token
from flask_migrate import Migrate
from flask_security import (RoleMixin, SQLAlchemySessionUserDatastore,
                            SQLAlchemyUserDatastore, UserMixin)
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import (Boolean, Column, DateTime, Enum, ForeignKey, Integer,
                        MetaData, String, Table, Text, UniqueConstraint)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship
from werkzeug.security import check_password_hash, generate_password_hash

# from mail import send_invite
from utils import random_string

migrate = Migrate()
db = SQLAlchemy()
meta = MetaData()

# Base = declarative_base()


class RolesUsers(db.Model):
    __tablename__ = "roles_users"
    id = Column(Integer(), primary_key=True)
    user_id = Column("user_id", Integer(), ForeignKey("users.id"))
    role_id = Column("role_id", Integer(), ForeignKey("roles.id"))


class Role(db.Model, RoleMixin):
    __tablename__ = "roles"
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))


class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    confirmed_at = Column(DateTime())
    username = Column(String(255), unique=True, nullable=True)
    password = Column(String(255), nullable=False)
    active = Column(Boolean())
    fs_uniquifier = Column(String(255), unique=True, nullable=False)

    last_login_at = Column(DateTime())
    last_login_ip = Column(String(100))
    current_login_at = Column(DateTime())
    current_login_ip = Column(String(100))
    login_count = Column(Integer)

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)

    roles = relationship(
        "Role",
        secondary="roles_users",
        backref=backref("users", lazy="dynamic"),
    )


class Car(db.Model):
    __tablename__ = "cars"
    id = Column(Integer, primary_key=True)
    slug = Column(String(20), unique=True)
    unknown = Column(Boolean())
    owner_id = Column(Integer, ForeignKey("owners.id"), nullable=True)
    title = Column(String(128))
    homepage_url = Column(String(128))
    referred = Column(String(128))
    last_known_location = Column(String(128))
    description = Column(Text(), default="")
    model = Column(String())
    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)

    last_good_info = Column(DateTime)

    images = relationship("CarImage", backref="car")

    def __repr__(self):
        return f"<Car {self.id}>"

    def to_dict(self):
        return {
            "id": self.id,
            "slug": self.slug,
            "model": self.model,
            "owner": {
                "name": self.owner.name,
                "email": self.owner.email,
            } if self.owner else None,
            "title": self.title,
            "homepage_url": self.homepage_url,
            "referred": self.referred,
            "last_known_location": self.last_known_location,
            "last_good_info": self.last_good_info,
            "updated_at": self.updated_at,
            "description": self.description,
        }


class CarImage(db.Model):
    __tablename__ = "car_images"
    id = Column(Integer, primary_key=True)
    slug = Column(String(40), unique=True, index=True)
    hero = Column(Boolean())
    car_id = Column(Integer, ForeignKey("cars.id"))

    path = Column(String(256))
    thumbnail = Column(String(256))
    description = Column(Text())

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)

    def __repr__(self):
        return f"<CarImage {self.path}>"

    def to_dict(self):
        return dict(
            slug=self.slug,
            path=self.path,
            hero=self.hero,
            thumbnail=self.thumbnail,
        )


class Owner(db.Model):
    __tablename__ = "owners"
    id = Column(Integer, primary_key=True)
    name = Column(String(128))
    email = Column(String(128), unique=True)
    cars = relationship("Car", backref="owner", lazy="dynamic")

    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)

    def __repr__(self):
        return f"<Owner {self.name}>"


user_datastore = SQLAlchemySessionUserDatastore(db.session, User, Role)
