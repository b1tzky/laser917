import os

import flask_admin
from flask import Flask, abort, redirect, render_template, request, url_for
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_admin.contrib import sqla
from flask_admin.contrib.sqla import ModelView
from flask_security import (RoleMixin, Security, SQLAlchemyUserDatastore,
                            UserMixin, current_user, login_required)
from flask_security.utils import encrypt_password
from flask_sqlalchemy import SQLAlchemy

from models import Car, db


class ProtectedModelView(ModelView):
    def is_accessible(self):
        return current_user.is_active and current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for("security.login", next=request.url))


admin = Admin(name="Laser917", template_mode="bootstrap3")
admin.add_view(ProtectedModelView(Car, db.session))
