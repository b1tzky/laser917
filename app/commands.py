import base64
import hashlib
import os
import pdb
import re
import shutil
from datetime import datetime
from glob import glob
from os.path import exists

import click
from flask import Blueprint
from flask_security import hash_password
from lxml import etree
from odf.opendocument import load
from PIL import Image, UnidentifiedImageError
from slugify import slugify

from models import Car, CarImage, Owner, db, user_datastore
from utils import resize_and_crop

commands = Blueprint("app", __name__)

def mkdir(*paths):
    for path in paths:
        try:
            os.mkdir(path)
        except FileExistsError:
            pass


def decorate_owner(owner, name=None, email=None):
    if owner:
        if name:
            owner.name = name
        if email:
            owner.email = email
    elif name or email:
        if existing_owner := Owner.query.filter_by(email=email).one_or_none():
            owner = existing_owner
        else:
            owner = Owner(name=name, email=email)
    return owner


@commands.cli.command("create_user")
@click.argument("email", required=False)
def create_user(email=None):
    email = email or click.prompt("email", type=str)

    if user := user_datastore.find_user(email=email):
        click.echo(f"{user.email} already exists")
        return

    user = user_datastore.create_user(
        email=email,
        password=hash_password(click.prompt("password",
            type=str,
            hide_input=True,
        ))
    )
    db.session.commit()
    click.echo(f"{user.email} created")


@commands.cli.command("import")
def index_cars():

    nsmap = {
        "office": "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
        "table": "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
        "text": "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
    }

    media_dir = f"./media"
    mkdir(media_dir)

    def first_or_none(results):
        try:
            return results[0]
        except Exception:
            return None

    for car_dir in glob("./cars/*/"):
        # get 917-___d59 from ./cars/917-___d59/
        slug = car_dir.split("/")[-2]

        car = Car.query.filter_by(slug=slug).one_or_none()
        if not car:
            car = Car(
                slug=slug,
                unknown=slug.split("-")[-1].startswith("___"),
            )
        click.echo(f"{car.slug}")

        if odt_file := glob(f"{car_dir}*.odt"):
            doc = load(odt_file[0])

            newdoc = f"{doc.xml()}"
            better_data = newdoc[newdoc.find("|") : newdoc.rfind("|")]
            # strip out random xml shit
            better_data = re.sub("<[^<]+>", "", better_data)

            data = etree.XML(doc.xml())

            if better_data:
                split = better_data.split("|")
                try:
                    car.owner = decorate_owner(
                        car.owner,
                        name=f"{split[4]} {split[5]}".strip() or None,
                        email=split[7].strip() or None,
                    )
                    car.last_good_info = datetime.strptime(
                        split[11], "%m/%d/%Y"
                    )
                    car.last_known_location = split[8]
                    # filter out blank entries before joining by comma
                    # car.description = str(
                    #     ", ".join(filter(lambda x: x, split[14:]))
                    # )
                except Exception as e:
                    pdb.set_trace()

            if table := first_or_none(
                data.xpath(
                    "office:body/office:text/table:table", namespaces=nsmap
                )
            ):

                def get_value(field_name, suffix="text()"):
                    if val := first_or_none(
                        table.xpath(
                            f"table:table-row/table:table-cell/text:p[text()='{field_name}']/parent::table:table-cell/following-sibling::table:table-cell[1]/text:p{suffix}",
                            namespaces=nsmap,
                        )
                    ):
                        return val.strip()
                    return None

                car.owner = decorate_owner(
                    car.owner,
                    name=get_value("Name:", "/text:p/text()"),
                    email=get_value("Email:", "/text:a/text:span/text()"),
                )
                car.title = get_value("Homepage Title:", "/text()")
                car.homepage_url = get_value("Homepage URL:", "/text()")
                car.referred = get_value("Referred By:", "/text()")
                car.last_known_location = get_value("Location:", "/text()")

                car.description = str(
                    "\n".join(
                        table.xpath(
                            f"table:table-row/table:table-cell/text:p[text()='Comments:']/parent::table:table-cell/following-sibling::table:table-cell[1]/text:p/text()",
                            namespaces=nsmap,
                        )
                    )
                )
            # else:
            #     click.echo(f"no ods table found")
            # if not car.owner.name and not car.owner.email:
            #     car.owner = None
            # pdb.set_trace()

            db.session.add(car)

        imgs = []
        valid_images = [".jpg", ".gif", ".png"]
        for i in os.listdir(car_dir):
            ext = os.path.splitext(i)[1]
            if ext.lower() in valid_images:
                imgs.append(i)

        if len(imgs):
            mkdir(f"{media_dir}/{slug}", f"{media_dir}/{slug}/t")

            for i in imgs:
                source_path = os.path.join(car_dir, i)
                media_path = f"{media_dir}/{slug}/{i}"
                thumb_path = f"{media_dir}/{slug}/t/{i}"

                try:
                    Image.open(source_path)
                except UnidentifiedImageError:
                    click.echo(f"couldnt open {source_path}")
                    continue

                if not exists(media_path):
                    try:
                        shutil.copy(source_path, media_path)
                    except Exception as e:
                        click.echo(str(e))
                        continue

                try:
                    resize_and_crop(source_path, thumb_path, (100, 100))
                except:
                    pdb.set_trace()

                hasher = hashlib.sha1(str.encode(f"{slug}/{i}"))
                hashed = (
                    base64.urlsafe_b64encode(hasher.digest())
                    .decode()
                    .rstrip("=")
                )
                if not CarImage.query.filter_by(
                    car_id=car.id, slug=hashed
                ).one_or_none():
                    image = CarImage(
                        car=car,
                        slug=hashed,
                        path=f"{slug}/{i}",
                        thumbnail=f"{slug}/t/{i}",
                    )
                    db.session.add(image)

        try:
            db.session.commit()
        except Exception as e:
            pdb.set_trace()
