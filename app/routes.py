from flask import Blueprint, render_template, send_from_directory
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_security import login_required

from models import Car

limiter = Limiter(
    key_func=get_remote_address,
    default_limits=["86400 per day", "20 per second"],
)


bp = Blueprint("routes", __name__, url_prefix="/")


@bp.route("/media/<path:name>", methods=["GET"])
def serve_media(name):
    return send_from_directory("./media/", name)


@bp.route("/car/<string:slug>", methods=["GET"])
@bp.route("", methods=["GET"])
# @login_required
def home(slug=None):
    return render_template("home.html"), 200


@bp.route("api/car/<string:slug>", methods=["GET"])
def car_detail(slug):
    if car := Car.query.filter_by(slug=slug).one_or_none():
        return (
            dict(
                car=car.to_dict(),
                images=[i.to_dict() for i in car.images],
            ),
            200,
        )
    return "notfound", 404
