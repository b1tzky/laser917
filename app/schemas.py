from flask_marshmallow import Marshmallow

from models import Car, User

ma = Marshmallow()


class CarSchema(ma.Schema):
    class Meta:
        model = Car
        fields = (
            "id",
            "title",
            "owner_name",
            "homepage_url",
            "last_known_location",
            "updated_at",
            "description",
            "images",
        )

car_schema = CarSchema()


class UserSchema(ma.Schema):
    class Meta:
        model = User
        fields = ("name", "icon", "date_created")

    # _links = ma.Hyperlinks(
    #     {
    #         "self": ma.URLFor("api.user_detail", values=dict(name="<name>")),
    #         # "collection": ma.URLFor("users"),
    #     }
    # )


user_schema = UserSchema()


# class ApplicantSchema(ma.Schema):
#     class Meta:
#         model = Applicant
#         fields = ("name", "application", "created_at")

#     # _links = ma.Hyperlinks(
#     #     {
#     #         "self": ma.URLFor("api.user_detail", values=dict(name="<name>")),
#     #         # "collection": ma.URLFor("users"),
#     #     }
#     # )

# applicant_schema = ApplicantSchema()


class MeSchema(ma.Schema):
    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "icon",
            "date_created",
            "threads_per_page",
            "comments_per_thread",
            "theme",
            "icon",
        )


me_schema = MeSchema()


# class ThreadSchema(ma.Schema):
#     class Meta:
#         model = Thread
#         fields = ("id", "title", "slug", "nsfw", "user", "created_at", "category")

#     category = ma.Function(lambda thread: thread.category.name)
#     user = ma.Nested(UserSchema(only=("name",)))

#     # _links = ma.Hyperlinks(
#     #     {
#     #         "self": ma.URLFor("api.user_detail", values=dict(name="<name>")),
#     #         # "collection": ma.URLFor("users"),
#     #     }
#     # )


# thread_schema = ThreadSchema()
# threads_schema = ThreadSchema(many=True)


# class CommentSchema(ma.Schema):
#     class Meta:
#         model = Comment
#         fields = ("id", "points", "content", "author")

# comments_schema = CommentSchema()
