from functools import cache

import flask
from dotenv import load_dotenv
from flask import current_app, render_template, url_for
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_security import (Security, SQLAlchemySessionUserDatastore,
                            login_required)
from sqlalchemy import desc
from werkzeug.local import LocalProxy
from whitenoise import WhiteNoise

load_dotenv()
logger = LocalProxy(lambda: current_app.logger)


from admin import admin
from models import Car, db, migrate, user_datastore
from routes import bp as routes_bp
from routes import limiter

security = Security()

from commands import commands


def create_app(**kwargs):
    app = flask.Flask(__name__)

    app.config.from_pyfile("config.py")
    app.config.update(kwargs)

    @security.context_processor
    def security_context_processor():
        return dict(hello="world")

    db.init_app(app)
    migrate.init_app(app, db)
    security.init_app(app, user_datastore)
    admin.init_app(app)
    limiter.init_app(app)

    app.wsgi_app = WhiteNoise(
        app.wsgi_app, root="static/dist/", prefix="static"
    )

    app.register_blueprint(routes_bp)
    app.register_blueprint(commands)

    @app.errorhandler(404)
    def not_found(error):
        return render_template("error.html", error=error, code="404"), 404

    @app.errorhandler(405)
    def method_not_allowed(error):
        return render_template("error.html", error=error, code="405"), 405

    # @app.errorhandler(422)
    # def unprocessable_entity(error):
    #     return error.data["messages"], 422
    #     return render_template("error.html", error=error, code="405"), 405
    #     # import pdb; pdb.set_trace()
    #     # messages = error.data["messages"].get("json")
    #     # return {
    #     #     "msg": "Sorry, no can do.",
    #     #     "fields": ["json"],
    #     # }, error.code

    @app.errorhandler(429)
    def rate_limited(error):
        return "too many requests", 429

    @app.context_processor
    @cache
    def inject_cars_index():
        cars = Car.query.filter_by(unknown=False).order_by(Car.slug)
        categoried = {}
        for car in cars:
            model = car.model or "None"
            if model not in categoried.keys():
                categoried[model] = []
            categoried[model].append(car.slug)
        return dict(cars_index=categoried)

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0")
